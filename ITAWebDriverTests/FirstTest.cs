using System;
using System.IO;
using System.Threading;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace ITAWebDriverTests
{
    [AllureNUnit]
    public class FirstTest
    {
        private const string ExpectedVideoTitle = "Massive volcanoes & Flamingo colony - Wild South America - BBC";
        private const string ExpectedResult = "BY";

        IWebDriver driver = new ChromeDriver();
        
        [Test]
        public void Test()
        {
            driver.Manage().Window.Maximize();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));

            driver.Navigate().GoToUrl("https://html.com/tags/iframe/");

            //driver.FindElement(By.Id("cn-accept-cookie")).Click();

            var iFrame = driver.FindElement(By.XPath("//div[@class='render']/iframe"));

            new Actions(driver).MoveToElement(iFrame).Build().Perform();

            Thread.Sleep(TimeSpan.FromSeconds(2));

            driver.SwitchTo().Frame(iFrame);

            var videoTitle = driver.FindElement(By.XPath("//a[@class='ytp-title-link yt-uix-sessionlink']"));

            if (videoTitle.Text.Equals(ExpectedVideoTitle))
            {
                var videoUrl = videoTitle.GetAttribute("href");
                driver.Navigate().GoToUrl(videoUrl);
            }

            Thread.Sleep(TimeSpan.FromSeconds(2));
            var actualResult = driver.FindElement(By.Id("country-code")).Text;

            Assert.AreEqual(ExpectedResult, actualResult);
        }

        [TearDown]
        public void Close()
        {
            driver.Quit();
        }
    }
}