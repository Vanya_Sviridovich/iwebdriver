using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace ITAWebDriverTests
{
    [AllureNUnit]
    public class SecondTest
    {
        private const string SearchKeys = "Selenium";
        private const string RepositoryName = "selenium";
        private string _pageSource;

        private const string ExpectedValue = "IHasInputDevices Interface";
        private bool _expectedResult;

        private IWebDriver _driver;

        [SetUp]
        public void SetUp()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
        }
        [Test]
        public void Test()
        {
            _driver.Navigate().GoToUrl("https://github.com");

            new Actions(_driver).MoveToElement(_driver.FindElement(By.XPath("//summary[contains(text(),'Why GitHub?')]"))).Build().Perform();

            _driver.FindElement(By.XPath("//a[contains(text(),'Actions')]")).Click();

            var input = _driver.FindElement(By.XPath("//input[@placeholder='Search GitHub']"));
            input.SendKeys(SearchKeys);
            input.Submit();
            
            var firstRespo = _driver.FindElements(By.XPath("//ul[@class='repo-list']//a")).First();

            new Actions(_driver).KeyDown(Keys.Control).MoveToElement(firstRespo).Click().Perform();

            _driver.SwitchTo().Window(_driver.WindowHandles.Last());

            _driver.Navigate().Refresh();

            var repositoryName =
                _driver.FindElement(By.XPath("//h1[contains(@class,\"public\")]/strong/a")).Text;

            if (repositoryName.Equals(RepositoryName))
            {
                _driver.Close(); 
                _driver.SwitchTo().Window(_driver.WindowHandles.Last());
                _driver.Navigate().GoToUrl("https://selenium.dev/selenium/docs/api/dotnet/");
                _pageSource = _driver.PageSource;
                StreamWriter file = new StreamWriter("pageSource.html");
                file.Write(_pageSource);
                file.Close();
                _expectedResult = _pageSource.Contains(ExpectedValue);
            }
            Assert.IsTrue(_expectedResult);
        }

        [TearDown]
        public void Close()
        {
            _driver.Quit();
        }
    }
}